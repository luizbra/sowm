#ifndef CONFIG_H
#define CONFIG_H

#define MOD Mod4Mask

const char* term[] = { "st", 0 };
const char* browser[] = { "chromium-browser", 0 };
const char* raisevol[] = { "pulsemixer", "--change-volume", "+5", 0 };
const char* lowervol[] = { "pulsemixer", "--change-volume", "-5", 0 };

static struct key keys[] = {
    { MOD,              XK_Return,                  run,        { .com = term } },
    { MOD,              XK_w,                       run,        { .com = browser } },
    { MOD,              XK_equal,                   run,        { .com = raisevol } },
    { MOD,              XK_minus,                   run,        { .com = lowervol } },
    { 0,                XF86XK_AudioRaiseVolume,    run,        { .com = raisevol } },
    { 0,                XF86XK_AudioLowerVolume,    run,        { .com = lowervol } },
    { MOD|ShiftMask,    XK_q,                       quit,       { 0 } },
    { MOD,              XK_q,                       win_kill,   { 0 } },
    { MOD,              XK_k,                       win_max,    { 0 } },
    { MOD,              XK_j,                       win_center, { 0 } },
    { MOD,              XK_h,                       win_left,   { 0 } },
    { MOD,              XK_l,                       win_right,  { 0 } },
    { MOD,              XK_Tab,                     win_next,   { 0 } },
    { MOD|ShiftMask,    XK_Tab,                     win_prev,   { 0 } },
    { MOD,              XK_1,                       ws_go,      { .i = 1 } },
    { MOD|ShiftMask,    XK_1,                       win_to_ws,  { .i = 1 } },
    { MOD,              XK_2,                       ws_go,      { .i = 2 } },
    { MOD|ShiftMask,    XK_2,                       win_to_ws,  { .i = 2 } },
    { MOD,              XK_3,                       ws_go,      { .i = 3 } },
    { MOD|ShiftMask,    XK_3,                       win_to_ws,  { .i = 3 } },
    { MOD,              XK_4,                       ws_go,      { .i = 4 } },
    { MOD|ShiftMask,    XK_4,                       win_to_ws,  { .i = 4 } },
};

#endif
